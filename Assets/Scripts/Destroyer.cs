﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {

    public float lifeTime;
    private float spawnTime;

    void Start()
    {
        spawnTime = Game.curTime;
    }

    void Update()
    {
        if(lifeTime > 0)
            if (Game.curTime - spawnTime > lifeTime)
                Destroy(gameObject);
    }

    //Для уничтожения анимаций
    void DestroyObject()
    {
        Destroy(gameObject);
    }
}
