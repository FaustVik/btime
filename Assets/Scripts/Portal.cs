﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {

    public int number;
    // Use this for initialization
    Renderer rend;

	void Start () {
        rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!Game.paused)
        {
            if (number == PortalTargetManager.curSelected)
            {
                //За некоторое время до смены на другой портал, начинаем мигать
                if (Game.curTime - PortalTargetManager.startTime >= PortalTargetManager.lifeTime * 0.75f)
                    SpriteBlinkingEffect();
                else
                    rend.material.color = Color.blue;
            }
            else
                rend.material.color = Color.white;
        }
	}

    public float spriteBlinkingTimer = 0.0f;
    public float spriteBlinkingMiniDuration = 0.1f;
    public float spriteBlinkingTotalTimer = 0.0f;
    public float spriteBlinkingTotalDuration = 2.0f;

    private void SpriteBlinkingEffect()
    {
        spriteBlinkingTimer += Time.deltaTime;
        if (spriteBlinkingTimer >= spriteBlinkingMiniDuration)
        {
            spriteBlinkingTimer = 0.0f;
            if (rend.material.color == Color.white)
                rend.material.color = Color.blue;
            else
                rend.material.color = Color.white;
        }
    }
}
