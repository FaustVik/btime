﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {

    private Text countText;
    private float timeToStart = 3;
    private float messageTime = 0.5f;
    private string startMessage = "Go!";
    private int fontSize;

    // Use this for initialization
    void Start()
    {
        if (Settings.countDisabled)
        {
            Game.paused = false;
            Destroy(gameObject);
        }

        countText = GetComponent<Text>();
        fontSize = countText.fontSize;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeToStart >= 0)
        {
            //Скидываем размер текста к оригинальному, если секунда сменилась
            if (Mathf.CeilToInt(timeToStart) != Mathf.CeilToInt(timeToStart - Time.deltaTime))
                countText.fontSize = fontSize;
            //Пишем время до старта
            timeToStart -= Time.deltaTime;
            countText.text = Mathf.CeilToInt(timeToStart) + "";
            countText.fontSize += 2;
        }
        else
        {
            //Анпаузим игру и выводим пол секунды наше приветственное сообщение
            if (messageTime >= 0)
            {
                messageTime -= Time.deltaTime;
                countText.text = startMessage;
                countText.fontSize += 2;
            }
            else
            {
                Game.paused = false;
                Destroy(gameObject);
            }
        }
    }
}
