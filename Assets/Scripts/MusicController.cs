﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {

    AudioSource musicPlayer;
    public static MusicController instance;

	// Use this for initialization
	void Start () {
        //Установим громкость музыки
        musicPlayer = GetComponent<AudioSource>();
        musicPlayer.ignoreListenerVolume = true;
        musicPlayer.volume = Settings.musicVolume;
        
        instance = this;
    }
	
    public void Pause()
    {
        musicPlayer.Pause();
    }

    public void UnPause()
    {
        musicPlayer.UnPause();
    }
}
