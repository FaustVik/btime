﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Game : MonoBehaviour {

    public static Game gameInstance; //Это наш синглтон (всё должно быть на нём, но как бы нет)
    public static bool paused; //На паузе ли сейчас игра
    public static float curTime; //Здесь будет наш счётчик времени

    //Линии
    public static int startline = 2; //линия, на которой стартуем
    public static int lineCount = 5; //Кол-во линий всего

    //Референсы
    static Transform player;
    public GameObject pauseMenu;
    public GameObject lostMenu;
    public Text newBest;
    public Text bestScore;
    public Text curScore;
    public Text coinsEarned;
    public Text coinsAmount;
    public GameObject easterMenu;
    public Button buttonShoot;
    public Button buttonUse;

    public static bool flipped; //перевёрнута ли игра

    private bool inMenu = false;

    void Start()
    {
        curTime = 0;
        paused = true;
        flipped = false;

        //Меняем местами кнопки стрелять и использовать, если игроку так удобней
        if(Settings.swapped)
        {
            Vector3 temp = buttonUse.transform.position;
            buttonUse.transform.position = buttonShoot.transform.position;
            buttonShoot.transform.position = temp;
        }

        AudioListener.volume = Settings.sfxVolume;

        //Синглтон
        gameInstance = this;
    }

    public static void Flip()
    {
        GameObject[] projectiles;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        projectiles = GameObject.FindGameObjectsWithTag("ProjectileTag");
        foreach (GameObject proj in projectiles)
        {
           Transform projTrans = proj.GetComponent<Transform>();
           projTrans.localScale *= -1;
        }
        player.localScale *= -1;
        flipped = !flipped;
    }

    public void Pause()
    {
        paused = true;
        MusicController.instance.Pause();
    }

    public void UnPause()
    {
        paused = false;
        MusicController.instance.UnPause();
        pauseMenu.SetActive(false);
    }

    public void ShowPauseMenu()
    {
        if (!inMenu && !paused)
        {
            Pause();
            pauseMenu.SetActive(true);
        }
    }

    public void ShowLostMenu()
    {
        Pause();
        inMenu = true;
        lostMenu.SetActive(paused);

        curScore.text = "" + ScoreManager.score;

        if (ScoreManager.score > ScoreManager.topScore)
        {
            newBest.gameObject.SetActive(true);
            ScoreManager.topScore = ScoreManager.score;
        }
    
        bestScore.text = "" + ScoreManager.topScore;

        int earned = ScoreManager.score / 100;
        coinsEarned.text = "+ " + earned;
        Player.coins += earned;

        coinsAmount.text = "" + Player.coins;

        //Почему бы и не пасхалки
        if (ScoreManager.score == 13370)
            easterMenu.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ShowPauseMenu();

        if (!paused)
        {
            //Наш счётчик времени, используем его повсюду просто:
            curTime += Time.deltaTime;

            if (Input.GetKeyDown(KeyCode.R))
                Restart();
        }
    }
}
