﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDestroyerManager : MonoBehaviour
{
    Rigidbody2D projectile;

    public float velocity = 3;

    public int direction = 2;

    // Use this for initialization
    void Start()
    {
        projectile = GetComponent<Rigidbody2D>();     
    }

    void Update()
    {
        if (Game.paused)
            projectile.velocity = Vector2.zero;
        else
        {
            switch (direction)
            {
                case 0:
                    projectile.velocity = new Vector2(0, velocity);
                    break;
                case 1:
                    projectile.velocity = new Vector2(velocity * 0.5f, velocity * 0.5f);
                    break;
                case 2:
                    projectile.velocity = new Vector2(velocity, 0);
                    break;
                case 3:
                    projectile.velocity = new Vector2(velocity * 0.5f, -velocity * 0.5f);
                    break;
                case 4:
                    projectile.velocity = new Vector2(0, -velocity);
                    break;
                case 5:
                    projectile.velocity = new Vector2(-velocity * 0.5f, -velocity * 0.5f);
                    break;
                case 6:
                    projectile.velocity = new Vector2(-velocity, 0);
                    break;
                case 7:
                    projectile.velocity = new Vector2(-velocity * 0.5f, velocity * 0.5f);
                    break;
            }
        }

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("ProjectileTag"))
        {
            if (!col.gameObject.GetComponent<ProjectileManager>().isFirst)
            {
                col.gameObject.GetComponent<ProjectileManager>().OnExplode();
                Destroy(col.gameObject);
            }
        }
    }
}
