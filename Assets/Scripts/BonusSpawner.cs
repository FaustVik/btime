﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpawner : MonoBehaviour {

    //Варианты наших бонусов
    public GameObject[] bonuses;

    //Переменные для референса на позиции порталов
    Transform portalFront;
    Transform portalBack;
    Transform spawnPos;

    private float bonusSpawningTime = 5f;
    public static float lastSpawnedBonus;

    // Use this for initialization
    void Start () {
        lastSpawnedBonus = 0;
        //Наши референсы для трасформа
        portalFront = GameObject.FindGameObjectWithTag("FrontPortal").transform;
        portalBack = GameObject.FindGameObjectWithTag("BackPortal").transform;
        spawnPos = GameObject.FindGameObjectWithTag("Respawn").transform;
    }

    bool CanSpawn(int randomPosition)
    {
        //Что на этой линии нет другого бонуса
        if (Physics2D.Raycast(new Vector2(Player.allowedPositions[randomPosition].x, portalFront.position.y), Vector2.down, 10, 1 << LayerMask.NameToLayer("Bonus")).collider != null)
            return false;

        return true;
    }

    void SpawnBonus()
    {
        int randomType;
        //Не спавним бонус, если не набрано нужное для его спавна кол-во очков
        do
            randomType = new System.Random().Next() % bonuses.Length;
        while (bonuses[randomType].GetComponent<BonusManager>().spawnsFrom > ScoreManager.score);

        BonusManager bMgr = bonuses[randomType].GetComponent<BonusManager>();

        //Не спавним бонусы на одной линии
        int randomPosition;
        do
            randomPosition = new System.Random().Next() % Player.allowedPositions.Count;
        while (!CanSpawn(randomPosition));

        //Высчитаем допустимый y относительно координат объектов порталов
        float randomPositionY;
        if (!Game.flipped)
            randomPositionY = Random.Range(spawnPos.position.y + bMgr.offset, portalFront.position.y - 0.2f);
        else
            randomPositionY = Random.Range(portalBack.position.y + 0.2f, spawnPos.position.y - bMgr.offset);

        //Создаём сам бонус
        Instantiate(bonuses[randomType], new Vector2(Player.allowedPositions[randomPosition].x, randomPositionY), Quaternion.identity);
        lastSpawnedBonus = Game.curTime;
    }

       

    // Update is called once per frame
    void Update () {
        if (!Game.paused)
        {
            if (!Player.isDead)
            {
                if (Game.curTime - lastSpawnedBonus > bonusSpawningTime)
                {
                    SpawnBonus();
                }
            }
        }
    }
    
}
