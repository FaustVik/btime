﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerGUI : MonoBehaviour {

    Transform player;
    Text targetCounter;
    public Text targetNeeded;

    public Vector3 offset;

    // Use this for initialization
    void Start () {
		player = GameObject.FindGameObjectWithTag("Player").transform;
        targetCounter = transform.Find("TargetCounter").GetComponent<Text>();
        targetNeeded.text = "/" + PortalTargetManager.targetsNeeded;
    }
	
	// Update is called once per frame
	void Update () {

        if(!Game.flipped)
            transform.position = player.position + offset;   //Следование за игроком
        else
            transform.position = player.position - offset;

        targetCounter.text = "" + PortalTargetManager.targetsHit;   //Изменение счётчика пораженных порталов
    }
}
