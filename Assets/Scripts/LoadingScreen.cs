﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour {

    public GameObject mainWindow;
    public GameObject loadingScreen;
    public Slider loadingSlider;
    public Text percentageText;
	
    public void LoadLevelAsync(string sceneName)
    {
        mainWindow.SetActive(false);
        loadingScreen.SetActive(true);

        StartCoroutine(Loading(sceneName));
    }

    IEnumerator Loading(string sceneName)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);

        while (!asyncOperation.isDone)
        {
            float progress = Mathf.Clamp01(asyncOperation.progress / .9f);
            percentageText.text = (int)(progress * 100) + " %";
            loadingSlider.value = progress;

            yield return null;
        }
    }
}
